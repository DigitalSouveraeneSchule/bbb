#!/usr/bin/bash
#
# generate munin config 
# 
#     ./munin-generator.sh > munin.conf.gen
#

set -eu

FIRST=3
LAST=350
SEQ="$(seq $FIRST 295) $(seq 300 349)" 

########

FIRSTf="$(printf "%03d" $FIRST)"

listall(){
    for i in $SEQ ; do
        N=$(printf "%03d" $i)
	H=$(( $i / 100 ))
        eval echo -e "$1" "$2" 
    done
    N=$(printf "%03d" $LAST)
    H=$(( $i / 100 ))
    eval echo -e "$1"
}


for i in $SEQ $LAST ; do
    N=$(printf "%03d" $i)
    H=$(( $i / 100 ))
    cat <<EOF
[containerhost${H}xx;b3srv${N}.lehrerfortbildung-bw.de]
    address b3srv${N}.lehrerfortbildung-bw.de
    if_vb_turn${N}.graph_title vb_turn traffic

EOF
done

cat <<EOF
[containerhost;01-totals]
###### numMeetings|numAttendees|numWithVoice|numWithVideo|numListeners ######
    update no
    numMeetingsSum.update no
    numMeetingsSum.graph_args -l 0
    numMeetingsSum.graph_category bbb
    numMeetingsSum.graph_title BBB Meeting Data: numMeetings
    numMeetingsSum.numMeetings.draw AREA
    numMeetingsSum.graph_order numMeetings numMeetingsSUM
    numMeetingsSum.numMeetings.stack \\
EOF
listall 'numMeetings_bbb${N}_SUM=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numMeetings.numMeetings_bbb${N}_SUM' '\\'
echo "    numMeetingsSum.numMeetingsSUM.sum \\"
listall 'containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numMeetings.numMeetings_bbb${N}_SUM' '\\'
cat <<EOF
    numMeetingsSum.numMeetingsSUM.label numMeetings-bbbSUM_SUM
    numMeetingsSum.numMeetingsSUM.draw LINE

    numAttendeesSum.update no
    numAttendeesSum.graph_args -l 0
    numAttendeesSum.graph_category bbb
    numAttendeesSum.graph_title BBB Meeting Data: numAttendees
    numAttendeesSum.graph_order numAttendees numAttendeesSUM
    numAttendeesSum.numAttendees.stack \\
EOF
listall 'numAttendees_bbb${N}_SUM=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numAttendees.numAttendees_bbb${N}_SUM' '\\'
cat <<EOF
    numAttendeesSum.numAttendees.draw AREA
    numAttendeesSum.numAttendeesSUM.sum \\
EOF
listall 'containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numAttendees.numAttendees_bbb${N}_SUM' '\\'
cat <<EOF
    numAttendeesSum.numAttendeesSUM.label numAttendees_bbbSUM_SUM
    numAttendeesSum.numAttendeesSUM.draw LINE

    numWithVoiceSum.update no
    numWithVoiceSum.graph_args -l 0
    numWithVoiceSum.graph_category bbb
    numWithVoiceSum.graph_title BBB Meeting Data: numWithVoice
    numWithVoiceSum.graph_order numWithVoice numWithVoiceSUM
    numWithVoiceSum.numWithVoice.stack \\
EOF
listall 'numWithVoice_bbb${N}_SUM=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numWithVoice.numWithVoice_bbb${N}_SUM' '\\'
cat <<EOF
    numWithVoiceSum.numWithVoice.draw AREA
    numWithVoiceSum.numWithVoiceSUM.sum \\
EOF
listall 'containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numWithVoice.numWithVoice_bbb${N}_SUM' '\\'
cat <<EOF
    numWithVoiceSum.numWithVoiceSUM.label numWithVoice_bbbSUM_SUM
    numWithVoiceSum.numWithVoiceSUM.draw LINE

    numWithVideoSum.update no
    numWithVideoSum.graph_args -l 0
    numWithVideoSum.graph_category bbb
    numWithVideoSum.graph_title BBB Meeting Data: numWithVideo
    numWithVideoSum.graph_order numWithVideo numWithVideoSUM
    numWithVideoSum.numWithVideo.stack \\
EOF
listall 'numWithVideo_bbb${N}_SUM=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numWithVideo.numWithVideo_bbb${N}_SUM' '\\'
cat <<EOF
    numWithVideoSum.numWithVideo.draw AREA
    numWithVideoSum.numWithVideoSUM.sum \\
EOF
listall 'containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numWithVideo.numWithVideo_bbb${N}_SUM' '\\'
cat <<EOF
    numWithVideoSum.numWithVideoSUM.label numWithVideo_bbbSUM_SUM
    numWithVideoSum.numWithVideoSUM.draw LINE

    numListenersSum.update no
    numListenersSum.graph_args -l 0
    numListenersSum.graph_category bbb
    numListenersSum.graph_title BBB Meeting Data: numListeners
    numListenersSum.graph_order numListeners numListenersSUM
    numListenersSum.numListeners.stack \\
EOF
listall 'numListeners_bbb${N}_SUM=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numListeners.numListeners_bbb${N}_SUM' '\\'
cat <<EOF
    numListenersSum.numListeners.draw AREA
    numListenersSum.numListenersSUM.sum \\
EOF
listall 'containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:bbb_numListeners.numListeners_bbb${N}_SUM' '\\'
cat <<EOF
    numListenersSum.numListenersSUM.label numListeners_bbbSUM_SUM
    numListenersSum.numListenersSUM.draw LINE


###### load ######  
    loadall.update no
    loadall.graph_category system
    loadall.graph_title Load average
    loadall.graph_order \\
EOF
listall 'load_bbb${N}=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:load.load' '\\'

cat <<EOF

###### fali2ban ######  
    fail2banall.update no
    fail2banall.graph_category network
    fail2banall.graph_title Hosts blacklisted by fail2ban
    fail2banall.graph_order \\
EOF
listall 'fail2ban_bbb${N}=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:fail2ban.sshd' '\\'

cat <<EOF

###### traffic virbr0 ######
    trafficall.update no
    trafficall.graph_category network
    trafficall.graph_title virbr0 traffic
    trafficall.graph_args --base 1000
    trafficall.graph_vlabel bits in (-) / out (+) per \${graph_period}
    trafficall.graph_order \\
EOF
listall 'downtraffic_bbb${N}=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:if_virbr0.down \
uptraffic_bbb${N}=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:if_virbr0.up' '\\'

cat <<EOF
trafficall.downtraffic_bbb${FIRSTf}.label received
trafficall.downtraffic_bbb${FIRSTf}.min 0
trafficall.uptraffic_bbb${FIRSTf}.min 0
EOF
listall 'trafficall.downtraffic_bbb${N}.graph no\\n\
trafficall.uptraffic_bbb${N}.label bps_${N}\\n\
trafficall.downtraffic_bbb${N}.cdef downtraffic_bbb${N},8,*\\n\
trafficall.uptraffic_bbb${N}.cdef uptraffic_bbb${N},8,*\\n\
trafficall.uptraffic_bbb${N}.negative downtraffic_bbb${N}' ''

cat <<EOF

###### traffic coturn ######
    trafturnall.update no
    trafturnall.graph_category network
    trafturnall.graph_title vb_turn traffic
    trafturnall.graph_args --base 1000
    trafturnall.graph_vlabel bits in (-) / out (+) per \${graph_period}
    trafturnall.graph_order \\
EOF
listall 'downtrafturn_bbb${N}=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:if_vb_turn${N}.down \
uptrafturn_bbb${N}=containerhost${H}xx\;b3srv${N}.lehrerfortbildung-bw.de:if_vb_turn${N}.up' '\\'

cat <<EOF
trafturnall.downtrafturn_bbb${FIRSTf}.label received
trafturnall.downtrafturn_bbb${FIRSTf}.min 0
trafturnall.uptrafturn_bbb${FIRSTf}.min 0
EOF
listall 'trafturnall.downtrafturn_bbb${N}.graph no\\n\
trafturnall.uptrafturn_bbb${N}.label bps_${N}\\n\
trafturnall.downtrafturn_bbb${N}.cdef downtrafturn_bbb${N},8,*\\n\
trafturnall.uptrafturn_bbb${N}.cdef uptrafturn_bbb${N},8,*\\n\
trafturnall.uptrafturn_bbb${N}.negative downtrafturn_bbb${N}' ''
