#!/bin/sh

set -eu

ARG="${1:-''}"

VOL=0.10
FADE="fade h 0.1 0.2 0.1"

for RATE in 8000 16000 32000 48000 ; do 
	sox -b 16 --rate $RATE -n -c1 outputC.wav   synth sin C 			$FADE vol $VOL
	sox -b 16 --rate $RATE -n -c1 outputCE.wav  synth sin C sin E 			$FADE vol $VOL
	sox -b 16 --rate $RATE -n -c1 outputCEG.wav synth sin C sin E sin G sin C5 	$FADE vol $VOL
	sox -b 16 --rate $RATE outputC.wav outputCE.wav outputCEG.wav conf-unmuted.wav
	sox -b 16 --rate $RATE outputCEG.wav outputCE.wav outputC.wav conf-muted.wav
	play conf-unmuted.wav -t alsa 
	play conf-muted.wav -t alsa 
	sleep 1
	if [ "$ARG" = "--overwrite" ] ; then
	       	cp -v conf-muted.wav conf-unmuted.wav ../roles/bbbcontainer/files/sounds/$RATE/
	else
		echo "To overwrite existing sounds, call $0 with '--overwrite' as argument."
	fi
done 
rm conf-muted.wav conf-unmuted.wav outputCEG.wav outputCE.wav outputC.wav

